<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\ToDo;

class AddTaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddTask()
    {
        $json = [
            'task' => 'Task R - '.time(),
            'completed' => false
        ];

        $client = new Client();
        $result = $client->put('localhost:8000/api/tasks', [
            'json' => $json
        ]);
        $statusCode = $result->getStatusCode();

        $this->assertEquals(201,$statusCode);
        $this->assertTrue(ToDo::where('task', $json['task'])->exists());
    }
}

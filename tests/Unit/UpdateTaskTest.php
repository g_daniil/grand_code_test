<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\ToDo;

class UpdateTaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdateTask()
    {
        $task = new ToDo(['task'=>'update me '.time()]);
        $task->save();

        $json = [
            'task' => 'updated '.time(),
            'completed' => 1
        ];

        $client = new Client();
        $result = $client->put('localhost:8000/api/tasks/'.$task->id, [
            'json'=>$json
        ]);

        $task = ToDo::find($task->id);

        $this->assertEquals($task->task, $json['task']);
        $this->assertEquals($task->completed, $json['completed']);
    }
}

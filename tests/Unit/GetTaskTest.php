<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\ToDo;

class GetTaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetTask()
    {

        $task = new ToDo(['task'=>'Test Get Task' . time()]);
        $task->save();

        $client = new Client();
        $result = $client->get('localhost:8000/api/tasks/'.$task->id);
        $task_from_request = $result->getBody();


        $task_from_db = ToDo::whereId($task->id)->get(['id','task','completed'])->first()->toJson();

        $this->assertEquals($task_from_request, $task_from_db);
    }
}

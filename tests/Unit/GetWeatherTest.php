<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GetWeatherTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetWeather()
    {
        $client = new Client();
        $result = $client->post('localhost:8000/api/weather/',[
            'form_params' => [
                'zipcode' => 11218
            ]
        ]);
        $statusCode = $result->getStatusCode();

        $this->assertEquals($statusCode, 200);
    }
}

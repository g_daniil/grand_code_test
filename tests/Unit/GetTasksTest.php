<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\ToDo;

class GetTasksTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetTasks()
    {

        $client = new Client();
        $result = $client->get('localhost:8000/api/tasks');
        $tasks_from_request = $result->getBody();


        $tasks_from_db = ToDo::get(['id','task','completed'])->toJson();

        $this->assertEquals($tasks_from_request,$tasks_from_db);
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GetWeatherWrongZipTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetWeatherWrongZip()
    {
        try {
            $client = new Client();
            $result = $client->post('localhost:8000/api/weather/',[
                'form_params' => [
                    'zipcode' => 11218232
                ]
            ]);
            $this->fail("Should throw an exception");
        } catch(GuzzleException $e) {
            $this->assertEquals($e->getCode(), 418);
        }
    }
}

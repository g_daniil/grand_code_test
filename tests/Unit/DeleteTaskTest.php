<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\ToDo;

class DeleteTaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteTask()
    {

        $toDo = new ToDo();
        $toDo->task = "Test Task For Deletion";
        $toDo->save();

        $id = $toDo->id;

        $client = new Client();
        $result = $client->delete('localhost:8000/api/tasks/'.$id);
        $statusCode = $result->getStatusCode();

        $this->assertEquals(200,$statusCode);
    }
}

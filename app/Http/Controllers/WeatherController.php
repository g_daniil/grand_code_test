<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class WeatherController extends Controller
{
    public function currentWeather(Request $request) {

        $APPID = 'afa85d2238bd60e7cdf19dee3c0aeb77';

        $zipcode = $request->zipcode;

        try{

            $client = new Client();
            $result = $client->get('api.openweathermap.org/data/2.5/weather',[
                'query' => [
                    'APPID' => $APPID,
                    'zip' => $zipcode
                ]
            ]);
    
            $weather = json_decode($result->getBody());

            return json_encode($weather->weather);

        } catch (GuzzleException $e) {
            if($e->getCode()==404) {
                return response('I\'m a teapot. Teapots do not know anything about weather',418);
            } else {
                return response($e->getMessage(), $e->getCode());
            }
        }


    }
}

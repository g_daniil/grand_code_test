<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ToDo;

class ToDoController extends Controller
{

    public function list()
    {
        $toDos = ToDo::get(['id','task','completed'])->toJson();
        return $toDos;
    }

    public function view($id)
    {
        $toDo = ToDo::whereId($id)->get(['id','task','completed'])->first();
        if(!$toDo) {
            return response('Not Found', 404 );
        }
        return $toDo->toJson();
    }

    public function save(Request $request, $id = null)
    {
        $params = $request->json();

        $new = false;

        $toDo = Todo::whereId($id)->first();

        if(!$toDo) {
            $toDo = new ToDo();
            $new = true;
        }

        try {
            $toDo->task = $params->get('task');
            $toDo->completed = $params->has('completed');
            $toDo->save();
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response('OK', $new ? 201 : 200 );
    }

    public function delete($id)
    {
        try {
            $task = Todo::findOrFail($id);
            $task->delete();
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
        return response('OK', 200);
    }
}

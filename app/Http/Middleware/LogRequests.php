<?php

namespace App\Http\Middleware;

use Closure;
use App\RequestLog;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $log = new RequestLog();
        $log->route = $request->getRequestUri();
        $log->request_method = $request->getMethod();
        $log->http_status_code = $response->getStatusCode();

        $log->save();

        return $response;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    
    protected $table = 'request_logs';

    protected $fillable = [
        'http_status_code',
        'request_method',
        'route'
    ];
}

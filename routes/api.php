<?php

use Illuminate\Http\Request;
use App\Http\Middleware\LogRequests;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'tasks', 'middleware' => LogRequests::class], function(){
    Route::get('/', 'ToDoController@list');
    Route::get('/{id}', 'ToDoController@view')->where('id', '[0-9]+');
    
    Route::put('/{id}', 'ToDoController@save')->where('id', '[0-9]+');
    Route::put('/', 'ToDoController@save');
    
    Route::delete('/{id}', 'ToDoController@delete')->where('id', '[0-9]+');
});

Route::post('/weather', 'WeatherController@currentWeather');

#######################################

1. Make sure that composer is installed
2. Run "composer install" in the root directory
3. Run "cp .env.example .env" in the root directory
4. Run "php artisan serve"

#######################################

To create task:
PUT 127.0.0.1:8000/api/tasks

To update task:
PUT 127.0.0.1:8000/api/tasks/{ID}

To delete task:
DELETE 127.0.0.1:8000/api/tasks/{ID}

To list all tasks:
GET 127.0.0.1:8000/api/tasks

To get 1 task:
GET 127.0.0.1:8000/api/tasks/{ID}

To get weather by zipcode:
POST 127.0.0.1:8000/api/weather
 - with field 'zipcode'

To run tests:
run command ".\vendor\bin\phpunit" in the root directory